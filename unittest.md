# __Unit Tests__

# Nội dung tài liệu

# Sơ bộ về Unit Tests

## Unit Tests là gì?

- Là phương pháp dùng để ___kiểm tra tính đúng đắn của một đơn vị source code___. Một Unit (đơn vị) source code là phần nhỏ nhất có thể test được của chương trình, ___thường là một phương thức trong một lớp hoặc một tập hợp các phương thức thực hiện một mục đích thiết yếu___.

- Là một tập con của automated test đáp ứng các tính chất sau:

    - _Nhanh chóng_: Unit tests sẽ hoàn thành trong vài mili giây.
    - _Nhất quán_: Với cùng một mã code, một unit test sẽ đưa ra kết quả giống nhau.
    - _Rõ ràng_: Một unit test fail phải chỉ ra rõ vấn đề dò tìm được.
- Viết các test case để Xcode tiến hành test các test case đó.

## Giới thiệu về Unit test trong iOS

Xcode cung cấp ___3 kiểu test chính___:

- _Functional test_: tập trung vào test các func.
- _Performance tests_: tập trung vào đo lượng thời gian app của bạn thực thi xong các task trên các loại thiết bị khác nhau.
- _User Interface tests (UI Tests)_: tập trung vào những tác vụ của người dùng trên UI.

## Useful Test

Một test case được coi là useful khi:

- Test case phải có khả năng fail: Nếu test đó không thể fail, thì việc test sẽ rất vô giá trị, bạn nên xốa nó đi.
- Test case phải có khả năng success: Nếu test đó không thể success, thì việc test sẽ rất vô giá trị, tương tự ở trên, bạn nên xốa nó đi.
- Test case phải được refactor và được viết đơn giản

## Thêm unit tests vào trong project

Có 2 trường hợp sau:

- Nếu dự án chưa được khởi tạo, tích vào Include Tests

    ![Include Tests](https://swdevnotes.com/images/swift/2021/0627/include-tests.png)

- Dự án đã được khởi tạo trước đó

    1. Lựa chọn __Test Navigator__ ở ngăn bên trái Xcode __(⌘6)__
    2. Nhấp vào __+__ ở dưới cùng bên trái và chọn __New Unit Test Target…__
    3. Trong hộp thoại xuất hiện, nhấp vào Kết thúc. 

        <img src="https://koenig-media.raywenderlich.com/uploads/2021/03/tests_1_Introduction_NewUnitTestTarget_annotated-1.png#center)" height="400">

## Run unit tests trong Xcode

Có thể run unit tests với 3 cách:
- Từ thanh menu với __Product__ > __Test__ hoặc sử dụng phím tắt: __⌘U__. 
- Nhấp vào nút mũi tên trong __Test Navigator__.
- Nhấp vào nút kim cương như trong hình.

    ![Run test](https://koenig-media.raywenderlich.com/uploads/2021/03/tests_3_Introduction_RunningTest_annotated.png)

    Khi tất cả các test thành công, các nút sẽ chuyển sang màu xanh lá cây và hiển thị check marks. Nhấp vào hình thoi màu xám ở cuối testPerformanceExample () để mở kết quả hiệu suất:

    ![Show performance](https://koenig-media.raywenderlich.com/uploads/2021/03/tests_4_Intorduction_PerformanceTest.png)

## Các thành phần trong Unit Tests Class mặc định

Gồm những thành phần sau đây:

```Swift
import XCTest
@testable import UnitTest

class MyUnitTests: XCTestCase {
    
    override func setUpWithError() throws {
    }

    override func tearDownWithError() throws {
    }

    func testPerformanceExample() throws {

        measure {
            // put your code to mesuare time here
        }
    }
    
}
```
- __XCTest__ là một Framework của Apple giúp tạo và chạy các unit tests, performance tests và UI Tests. Trong file test được tạo đã import sẵn __XCTest Framework__ và class Test extends __XCTestCase__

    ```Swift
    class AssertYourselfTests: XCTestCase {
    }
    ```
- __@testable__ được sử dụng khi cần khai báo extra import
- __func setUp()__: dùng để thiết lập lại trạng thái trước mỗi lần test.
- __func tearDown()__: dùng để thực hiện dọn dẹp sau khi mỗi lần test kết thúc.
- __measure__: dùng để đo thời gian thực hiện xong việc test -> giúp test performance.

Note: Tìm hiểu kĩ hơn về setup() và tearDown() trong phần [Quản lý Lifecycle của test](#quản_lý_Lifecycle_của_test)

# Cách thức để viết Unit Tests

## Viết unit test với XCTest

- Sử dụng Framework __XCTest__ để viết các unit tests cho các dự án Xcode tích hợp liền mạch với quy trình kiểm tra của Xcode.

- Tạo các func test

    Trong __Class Test__, định nghĩa ra các __func test__ là các test case cho unit tests, __func test__ nên thỏa mãn các điều kiện sau:
    - Nằm trong một lớp con của __XCTestCase__. 
    - Không khai báo là __private__. 
    - Tên nên bắt đầu bằng __test__. 
    - Không cần tham số. 
    - Không có giá trị trả lại.

        ```Swift
        struct Student {
            let name: String
            let grades: [Float]
        }

        class StudentManager {
            func getGpa(student: Student) -> Float? {
                guard !student.grades.isEmpty else { return nil }
                return student.grades.reduce(0, +) / Float(grades.count)
            }
        }
        ```

        ```Swift
        import XCTest
        @testable import UnitTest

        class StudentTests: XCTestCase {
            //1
            var sut: StudentManager!
            
            override func setUp() {
                super.setUp()
                //2
                sut = StudentManager()
            }

            override func tearDown() {
                //3
                sut = nil
                super.tearDown()
            }
            
            //test1
            func testGPAWithNoGrades() {
                let student = Student(name: "test", grades: [])
                let gpa = sut.getGpa(student)
                //4
                XCTAssertNil(gpa)
            }
            
            //test2
            func testGPAWithOneGrade() {
                let student = Student(name: "test", grades: [4])
                let gpa = sut.getGpa(student)
                //5
                XCTAssertEqual(gpa, 4)
            }

            //test3
            func testGPAWithTenGrades() {
                let student = Student(name: "test", grades: [1, 2, 3, 4, 5, 6, 7, 8, 9, 10])
                let gpa = sut.getGpa(student)
                XCTAssertEqual(gpa, 5.5)
            }
        }
        ```
        
        - 1 - Khai báo 1 biến kiểu __StudentManager__.
        - 2 - Khởi tạo __sut__ mỗi lần bắt đầu thực hiện test 1 test case.
        - 3 - Set __sut = nil__ mỗi khi kết thúc việc test 1 test case.
        - 4 - Xác định giá trị __nil__
        - 5 - Xác định 2 biểu thức có cùng giá trị
        - test1, test2, test3 - các func test để test func getGpa trong class StudentManager. 

## Test Assertions

- Boolean Assertion: Gồm XCTAssert, XCTAssertTrue, XCTAssertFalse 
- Nil và Non-Nil Assertions: Gồm XCTAssertNil XCTAssertNotNil, XCTUnwrap
- Equality and Inequality Assertions: Gồm XCTAssertEqual, XCTAssertNotEqual
- Comparable Value Assertion: XCTAssertGreaterThan, XCTAssertGreaterThanOrEqual, XCTAssertLessThan, XCTAssertLessThanOrEqual

| Assertion | Mục đích |
| --------- | ------- |
| func XCTAssert(() -> Bool, () -> String, file: StaticString, line: UInt) | Khẳng định rằng biểu thức là đúng | 
| func XCTAssertTrue(() -> Bool, () -> String, file: StaticString, line: UInt) | Khẳng định rằng biểu thức là đúng |
| func XCTAssertFalse(() -> Bool, () -> String, file: StaticString, line: UInt) | Khẳng định rằng biểu thức là sai |
| func XCTAssertNil(() -> Any?, () -> String, file: StaticString, line: UInt) | Khẳng định rằng một giá trị optional có kết quả là nil |
| func XCTAssertNotNil(() -> Any?, () -> String, file: StaticString, line: UInt) | Khẳng định rằng một giá trị optional có kết quả là khác nil |
| func XCTAssertEqual<T>(() -> T, () -> T, () -> String, file: StaticString, line: UInt) | Khẳng định rằng 2 giá trị bằng nhau |
| func XCTAssertEqual<T>(() -> T, () -> T, accuracy: T, () -> String, file: StaticString, line: UInt) | Khẳng định 2 giá trị float bằng nhau với một khoảng lớn hơn hoặc nhỏ hơn _accuracy_ |
| func XCTAssertNotEqual<T>(() -> T, () -> T, () -> String, file: StaticString, line: UInt) | Khẳng định rằng hai giá trị không bằng nhau |
| func XCTUnwrap <T> (() -> T ?, () -> String, file: StaticString, line: UInt) -> T | Khẳng định rằng một biểu thức không phải là nil và trả về giá trị unwrapped
| func XCTAssertGreaterThan<T>(() -> T, () -> T, () -> String, file: StaticString, line: UInt) | Khẳng định rằng giá trị của biểu thức đầu tiên lớn hơn giá trị của biểu thức thứ hai |
| func XCTAssertGreaterThanOrEqual<T>(() -> T, () -> T, () -> String, file: StaticString, line: UInt) | Khẳng định rằng giá trị của biểu thức đầu tiên lớn hơn hoặc bằng giá trị của biểu thức thứ hai |
| func XCTAssertLessThanOrEqual<T>(() -> T, () -> T, () -> String, file: StaticString, line: UInt) | Khẳng định rằng giá trị của biểu thức đầu tiên nhỏ hơn hoặc bằng giá trị của biểu thức thứ hai |
| func XCTAssertLessThan<T>(() -> T, () -> T, () -> String, file: StaticString, line: UInt) | Khẳng định rằng giá trị của biểu thức đầu tiên nhỏ hơn giá trị của biểu thức thứ hai |
| func XCTAssertFail() | Khẳng định rằng một biểu thức là sai, không thực hiện được test hiện tại. Nên cung cấp một thông điệp mô tả |

## Một số lưu ý khi viết 

1. Thêm một thông điệp mô tả

    Biết vị trí của một test không thành công là một khởi đầu tốt. Nhưng khi một test không thành công, phải chẩn đoán những gì đã xảy ra. Có thể tiết kiệm thời gian bằng cách giải thích bất cứ điều gì chúng ta biết tại thời điểm thất bại.

    ```Swift
    func testFailƯithSimpleMessage() {
        XCTFail("We have a problem")
    }
    ```
    > failed - We have a problem

    __XCTFail()__ có thể làm được nhiều việc hơn là chỉ ra một chuỗi ký tự. 

    ```Swift
    func testFailWithInterpolatedMessage() {
        let theAnswer = 20
        XCTFail("The Answer to the Great Question is \(theAnswer)")
    }
    ```

    > failed - The Answer to the Great Question is 20

2. Tránh các điều kiện trong test

    Thay vì viết
    
    ```Swift
    func testAvoidConditionalCode() {
        let success = false
        if !success {
        XCTFail()
        }
    }
    ```
    Ta viết theo cách sau

    ```Swift
    func testAssertTrue() {
        let success = false
        XCTAssertTrue(success)
    }
    ```

    Bằng cách sử dụng các Boolean Assertion __XCTAssertTrue()__ và __XCTAssertFalse()__, chúng ta có thể tránh nhiều điều kiện trong mã code

3. Mô tả các đối tượng khi bị lỗi

    Đây là Assertion để xác nhận rằng giá trị optional là __nil__. Thêm test này và chạy thử:
    ```Swift
    func testAssertNil() {
        let optionalValue: Int? = 123
        XCTAssertNil(optionalValue)
    }
    ```

    > XCTAssertNil failed: "123"

    Thử thay kiểu __Int__ bằng __struct__

    ```Swift
    struct SimpleStruct {
        let x: Int
        let y: Int
    }

    func testAssertNilWithSimpleStruct() {
        let optionalValue: SimpleStruct? = SimpleStruct(x: 1, y: 2)
        XCTAssertNil(optionalValue)
    }
    ```
    > XCTAssertNil failed: "SimpleStruct(x: 1, y: 2)" 

    Khá dễ đọc đối với một __struct__. Nhưng một số loại có mô tả phức tạp. Điều này có thể làm cho các thông báo lỗi khó đọc. Có thể kiểm soát cách một loại mô tả chính nó bằng cách làm cho nó phù hợp với __CustomStringConvertible__.

    ```Swift
    struct StructWithDescription: CustomStringConvertible {
        let x: Int
        let y: Int
        var description: String { "(\(x), \(y))" }
    }

    func testAssertNilWithSelfDescribingType() {
        let optionalValue: StructWithDescription? =
        StructWithDescription(x: 1, y: 2)
        XCTAssertNil(optionalValue)
    }
    ```
    > XCTAssertNil failed: "(1, 2)"

4. Test Equality

- __Test Equality__ thông thường

    Loại Assertion phổ biến nhất là kiểm tra một kết quả xem có bằng kết quả mong đợi hay không.

    ```Swift
    func testAssertEqual() {
        let actual = "actual"
        XCTAssertEqual(actual, "expected")
    }
    ```
    > XCTAssertEqual failed: ("actual") is not equal to ("expected")

    Cần lưu ý rằng các unit test Framework khác thường sử dụng (expected, actual). Thứ tự quan trọng vì thông báo lỗi cho biết đó là định dạng sau:

    > expected: <"expected"> but was: <"actual">

    Nhưng với __XCTAssertEqual()__, thứ tự đối số không quan trọng. Nó chỉ đơn giản là báo ("A") không bằng ("B"). Vì có thể sắp xếp chúng theo bất kỳ thứ tự nào, như sau

    ``XCTAssertEqual("expected", actual)``

- __Test Equality__ với __Optionals__

    ```Swift
    func testAssertEqualWithOptional() {
        let result: String? = "foo"
        XCTAssertEqual(result, "bar")
    }
    ```
    > XCTAssertEqual failed: ("Optional("foo")") is not equal to ("Optional("bar")")

    Ta nhập một chuỗi đơn giản là "bar" làm đối số thứ hai. Làm thế nào nó trở __Optional__? __XCTAssertEqual()__ yêu cầu cả hai đối số phải cùng kiểu. Swift biết rằng nếu một giá trị kiểu __T__ đang được gán cho một biến kiểu __T?__, nó có thể wrap nó. Điều này giải thích cho việc từ kiểu __non-optional__ thành __optional__.

- __Tests Equality__ với một __Accuracy__ cho trước

    ```Swift
    func testFloatingPointEqually() {
        let result = 0.1 + 0.2
        XCTAssertEqual(result, 0.3)
    }
    ```
    > XCTAssertEqual failed: ("0.30000000000000004") is not equal to ("0.3") 

    Nên thay thế bởi

    ```Swift
    func testFloatingPointFixed() {
        let result = 0.1 + 0.2
        XCTAssertEqual(result, 0.3, accuracy: 0.0001)
    }
    ```

5. Chọn đúng Assertion

    Làm thế nào để chọn cái nào để sử dụng cho một test cụ thể? Tham khảo lại mục [Test Assertions](#test_assertions)

# Quản lý Lifecycle của test

## Ta dùng đoạn code sau để tìm hiểu về lifecycle của test. 

Dùng 2 method để print ra console, không dùng để test assertions.

```Swift
class MyClass {
    func methodOne() {
        print(">> methodOne")
    }
    func methodTwo() {
        print(">> methodTwo")
    }
}   
```

## Tạo một class Test

```Swift
import XCTest
class MyClassTests: XCTestCase {
}
```

Viết func test

```Swift
func test_methodOne() {
    let sut = MyClass()
    sut.methodOne()
    // Normally, assert something
}

func test_methodTwo() {
    let sut = MyClass()
    sut.methodTwo()
    // Normally, assert something
}
```
sut là viết tắt của system under test, thường được viết tắt là SUT, là một thuật ngữ phổ biến. Không giống như ví dụ đơn giản này, các test thường có nhiều object. Việc sử dụng một tên nhất quán như sut giúp làm rõ đối tượng mà thử nghiệm sẽ thực hiện. Nó cũng giúp việc sử dụng lại các đoạncode dễ dàng hơn.

Test sẽ không complie. Nó không biết MyClass là gì. Chúng ta cần thêm dòng này ở đầu tệp:

```Swift
@testable import LifeCycle
```

Tại sao chúng ta cần thuộc tính @testable trên câu lệnh nhập? Đó là bởi vì không chỉ định access control cho MyClass, vì vậy nó được đặt mặc định là truy cập nội bộ. @testable hiển thị các khai báo internal. Lưu ý rằng mọi thứ được khai báo là private vẫn nằm ngoài giới hạn, ngay cả đối với các test. Test bây giờ sẽ được build và run.

Ta có đoạn log sau

> Test Suite 'All tests' started at 2022-04-19 19:08:40.422

> Test Suite 'LifeCycleTests.xctest' started at 2022-04-19 19:08:40.422

> Test Suite 'MyClassTests' started at 2022-04-19 19:08:40.423

> Test Case '-[LifeCycleTests.MyClassTests test_methodOne]' started.
    
> Test Case '-[LifeCycleTests.MyClassTests test_methodOne]' passed (0.002 seconds).

> Test Case '-[LifeCycleTests.MyClassTests test_methodTwo]' started.
    
> Test Case '-[LifeCycleTests.MyClassTests test_methodTwo]' passed
(0.000 seconds).

> Test Suite 'MyClassTests' passed at 2022-04-19 19:08:40.426.
Executed 2 tests, with 0 failures (0 unexpected) in 0.002 (0.003)
seconds

> Test Suite 'LifeCycleTests.xctest' passed at 2022-04-19 19:08:40.426.

> Executed 2 tests, with 0 failures (0 unexpected) in 0.002 (0.004)
seconds

> Test Suite 'All tests' passed at 2022-04-22 19:08:40.426.
Executed 2 tests, with 0 failures (0 unexpected) in 0.002 (0.005)
seconds

- Quan sát Object Life Cycles để tìm hiểu các giai đoạn của Test. Thêm 2 phương thức init() và deinit()

```Swift
class MyClass {
    private static var allInstances = 0
    private let instance: Int

    init() {
        MyClass.allInstances += 1
        instance = MyClass.allInstances
        print(">> MyClass.init() #\(instance)")
    }

    deinit {
        print(">> MyClass.deinit #\(instance)")
    }

    func methodOne() {
        print(">> methodOne")
    }

    func methodTwo() {
        print(">> methodTwo")
    }
}
```

Điều này giữ số lượng các instances MyClass đang chạy, in thông báo khi init() và deinit() được gọi.
Sau đó, ta thêm assertions XCTFail () vào
kết thúc testMethodOne (). 

```XCTFail ("Không thành công")```

```
Test Case '-[LifeCycleTests.MyClassTests test_methodOne]' started.
>> MyClass.init() #1
>> methodOne
 LifeCycle/LifeCycleTests/MyClassTests.swift:10: error:
-[LifeCycleTests.MyClassTests test_methodOne] : failed - Không thành công
>> MyClass.deinit #1
Test Case '-[LifeCycleTests.MyClassTests test_methodOne]' failed
(0.005 seconds).
```
Điều này cho thấy rằng đối với testMethodOne(), điều sau xảy ra: 
1. Kiểm tra tạo ra một instance của MyClass. 
2. Nó gọi một phương thức của instance. 
3. Nó assert một kết quả. 
4. Nó giải phóng đi instance.

## Cách thức sai để cắt giảm code trùng lặp

```Swift
class MyClassTests: XCTestCase {
    func test_methodOne() {
        let sut = MyClass()
        sut.methodOne()
        // Normally, assert something
    }

    func test_methodTwo() {
        let sut = MyClass()
        sut.methodTwo()
        // Normally, assert something
    }
}
```

Một số sẽ nghĩ, "Tại sao không đưa sut từ các biến cục bộ thành một thuộc tính?" Nó sẽ trông như thế này:

```Swift
class MyClassTests: XCTestCase {
    private let sut = MyClass()
    func test_methodOne() {
        sut.methodOne()
        // Normally, assert something
    }

    func test_methodTwo() {
        sut.methodTwo()
        // Normally, assert something
    }
}
```

Vậy vấn đề là gì? Chạy các bài kiểm tra và xem bảng kiểm tra. Nếu bạn đi sâu vào cấp độ "All tests", bạn sẽ thấy như thế này:

```
>> MyClass.init() #1
>> MyClass.init() #2
Test Suite 'All tests' started at 2022-04-19 20:12:15.128
Test Suite 'LifeCycleTests.xctest' started at 2022-04-19 20:12:15.128
Test Suite 'MyClassTests' started at 2022-04-19 20:12:15.129
Test Case '-[LifeCycleTests.MyClassTests test_methodOne]' started.
>> methodOne
Test Case '-[LifeCycleTests.MyClassTests test_methodOne]' passed
(0.001 seconds).
Test Case '-[LifeCycleTests.MyClassTests test_methodTwo]' started.
>> methodTwo
Test Case '-[LifeCycleTests.MyClassTests test_methodTwo]' passed
(0.000 seconds).
Test Suite 'MyClassTests' passed at 2022-04-19 20:12:15.131.
Executed 2 tests, with 0 failures (0 unexpected) in 0.001 (0.002)
seconds
Test Suite 'LifeCycleTests.xctest' passed at 2022-04-19 20:12:15.131.
Executed 2 tests, with 0 failures (0 unexpected) in 0.001 (0.003)
seconds
Test Suite 'All tests' passed at 2022-04-19 20:12:15.131.
Executed 2 tests, with 0 failures (0 unexpected) in 0.001 (0.004)
seconds
```

Tìm log của các lệnh gọi init và deinit. 
Hai instance của MyClass được tạo trước khi chạy test. Và chúng không bao giờ bị destroy. 

Các vấn đề có thể nhân lên: 
- Nếu có sự cố khi tạo MyClass, nó sẽ xảy ra trước khi bất kỳ test nào chạy. 
- Nếu có vấn đề gì khi hủy nó, sẽ không bao giờ biết vì các instance không bị hủy. 
- Nếu việc khởi tạo MyClass có bất kỳ tác động nào trên global, chẳng hạn như các phương pháp xáo trộn hoặc truy cập vào hệ thống tệp.
- Tìm hiểu cách XCTest quản lý các test cases.

Có thể dễ dàng giả định rằng khi XCTest chạy một test case, ba điều xảy ra: 
- Nó tạo ra một instance của lớp con XCTestCase. 
- Nó chạy test func cụ thể. 
- Nó destroy instance của XCTestCase. 
    
Hoặc, có thể đã giả định rằng XCTest tạo một instance để chạy tất cả các test. Nhưng cả hai đều không chính xác. Đây là những gì thực sự xảy ra: 
- XCTest tìm kiếm tất cả các lớp kế thừa từ XCTestCase. 
- Đối với mỗi class như vậy, nó tìm thấy mọi method test. Đây là những func có tên bắt đầu bằng test, không có đối số và không có giá trị trả về. 
- Đối với mỗi func test như vậy, nó tạo ra một instance của class. Nó ghi nhớ func test mà phiên bản đó sẽ chạy. 
- XCTest thu thập các instance của class con vào một bộ test.
- Khi hoàn tất việc tạo tất cả các test cases, chỉ khi đó XCTest mới bắt đầu chạy chúng.

Điều này có nghĩa là gì đối với ví dụ trên. Nó tìm kiếm các tên func bắt đầu bằng “test” và nó tìm thấy hai. Vì vậy, nó tạo ra hai phiên bản MyClassTests: một phiên bản để chạy testMethodOne(), một phiên bản khác để chạy testMethodTwo(). Và nó tập hợp các phiên bản này thành một bộ test trước khi chạy bất kỳ test nào. Vì mỗi phiên bản có thuộc tính MyClass đã vô tình tạo ra hai phiên bản MyClass. 

## Sử dụng setUp() và dropsDown() 

XCTestCase cung cấp các phương pháp đặc biệt để sử dụng. XCTestCase định nghĩa hai phương thức, setUp() và drawDown(). Chúng được thiết kế để được ghi đè trong các lớp con. Kết hợp với việc sử dụng các tùy chọn, ta thay thế như sau:

```Swift
class MyClassTests: XCTestCase {
    private var sut: MyClass!

    override func setUp() {
        super.setUp()
        sut = MyClass()
    }

    override func tearDown() {
        sut = nil
        super.tearDown()
    }

    func test_methodOne() {
        sut.methodOne()
        // Normally, assert something
    }

    func test_methodTwo() {
        sut.methodTwo()
        // Normally, assert something
    }
}
```

Chạy các bài test và kiểm tra log. Bạn sẽ thấy rằng XCTest đã tạo và hủy các instance MyClass trong mỗi lần chạy test. 

```private var sut: MyClass!```

Dấu "__!__" ở đây được sử dụng giống như đối với IBOutlet. XCTest là một Framework, có nghĩa là nó gọi lại code. Test runner trong XCTest đảm bảo trình tự sau cho mỗi trường hợp thử nghiệm: 

- Call setUp().
- Call the test method.
- Call tearDown().

Lưu ý rằng chỉ setUp() là không đủ. XCTest tạo các testcases, nhưng nó không bao giờ phá hủy chúng. Các thuộc tính của chúng sẽ tồn tại, vì vậy chúng ta cần có phương thức tearDown() để dọn dẹp phần còn lại của các phần được chia sẻ.

# Sử dụng XCTestExpectation để kiểm tra các hoạt động bất đồng bộ

Khi bắt đầu làm việc với các test, có một vấn đề là kiểm tra code không đồng bộ. Nó có thể là mã code thực hiện các yêu cầu mạng, thực hiện công việc trên nhiều luồng hoặc lên lịch cho các hoạt động bị trì hoãn.

Điều này không chỉ có thể làm cho mã code khó kiểm tra mà còn có thể dẫn đến kết không đúng. Hãy xem một ví dụ sau để test xem ImageScaler đang trả về số lượng scaled image:

``` Swift
class ImageScalerTests: XCTestCase {
    func testScalingProducesSameAmountOfImages() {
        let scaler = ImageScaler()
        let originalImages = loadImages()

        scaler.scale(originalImages) { scaledImages in
            XCTAssertEqual(scaledImages.count, originalImages.count)
        }
    }
}
```

Vấn đề là ImageScaler luôn thực hiện công việc của nó một cách không đồng bộ, có nghĩa là closure sẽ được thực thi quá muộn. Vì vậy, ngay cả khi assertion thất bại, bài test vẫn sẽ luôn vượt qua.

- Một cách phổ biến để giải quyết các vấn đề như trên là sử dụng các __expectation__. Về cơ bản là cung cấp một cách để nói với runner rằng bạn muốn đợi một lúc trước khi tiếp tục. Hãy cập nhật. Mã code được thay đổi như sau:

    ``` Swift
    class ImageScalerTests: XCTestCase {
        func testScalingProducesSameAmountOfImages() {
            let scaler = ImageScaler()
            let originalImages = loadImages()

            // Create an expectation
            let expectation = self.expectation(description: "Scaling")
            var scaledImages: [UIImage]?

            scaler.scale(originalImages) {
                scaledImages = $0

                // Fullfil the expectation to let the test runner
                // know that it's OK to proceed
                expectation.fulfill()
            }

            // Wait for the expectation to be fullfilled, or time out
            // after 5 seconds. This is where the test runner will pause.
            waitForExpectations(timeout: 5, handler: nil)

            XCTAssertEqual(scaledImages?.count, originalImages.count)
        }
    }
    ```

    Sử dụng các expectation khá dễ dàng để viết các test với các API không đồng bộ vì có thể đơn giản đáp ứng expectation trong closure đó. Nó thường phù hợp với mã code mà không thể thực sự tăng tốc và cần phải đợi - mà không cần phải đưa ra thời gian chờ không cần thiết, vì có thể tiến hành ngay sau khi hoạt động bất đồng bộ hoàn thành.

- Sử dụng __Inverted expectations__

    Điều thú vị là kỳ vọng không chỉ hữu ích cho việc chờ đợi điều gì đó xảy ra, chúng còn có thể được sử dụng để xác minh rằng điều gì đó đã không xảy ra.

    Giả sử test cho một lớp Debouncer để dễ dàng trì hoãn việc closure (ví dụ: nếu đang triển khai giao diện người dùng tìm kiếm khi nhập và không muốn thực hiện quá nhiều network request). Để kiểm tra xem Debouncer có thực sự trì hoãn các closure hay không và nó có hủy mọi lần closure đang chờ xử lý khi một closure mới được lên lịch hay không, chúng ta có thể sử dụng Inverted expectations. Việc Inverted expectations cũng dễ dàng như đặt thuộc tính isInverted thành true và nó hoạt động giống như mong đợi - test sẽ không thành công trong trường hợp expectation được thực hiện trong khung thời gian chót. Kết hợp Inverted expectation với expectation, chúng ta có thể viết một test như sau:

    ``` Swift
    class DebouncerTests: XCTestCase {
        func testPreviousClosureCancelled() {
            let debouncer = Debouncer(delay: 0.25)

            // Expectation for the closure we'e expecting to be cancelled
            let cancelExpectation = expectation(description: "Cancel")
            cancelExpectation.isInverted = true

            // Expectation for the closure we're expecting to be completed
            let completedExpectation = expectation(description: "Completed")

            debouncer.schedule {
                cancelExpectation.fulfill()
            }

            // When we schedule a new closure, the previous one should be cancelled
            debouncer.schedule {
                completedExpectation.fulfill()
            }

            // We add an extra 0.05 seconds to reduce the risk for flakiness
            waitForExpectations(timeout: 0.3, handler: nil)
        }
    }
    ```

    Có thể thấy ở trên, ta không thực sự cần phải tự mình thực hiện bất kỳ assertion nào, vì XCTest sẽ tự động không đạt các test trong trường hợp không đáp ứng được expectation.

- Khi dùng __dispatch queues__

    Mặc dù các expectation rất mạnh mẽ và hữu ích trong nhiều trường hợp khác nhau, nhưng đôi khi ta không có completion handler hoặc callback để kết nối, chẳng hạn như preload nội dung. Giả sử ta đang viết các bài test cho FileLoader, có một API để preload một tập hợp các tệp trên background queue. Thông thường, sẽ thực sự khó biết khi nào một hoạt động như vậy kết thúc và việc thêm toàn bộ API xử lý hoàn thành chỉ để test có vẻ hơi sai. Nhưng những gì chúng ta có thể làm là cho phép một DispatchQueue được đưa vào từ bên ngoài - theo cách đó chúng ta có thể sử dụng phương thức đồng bộ hóa để đưa ra một đóng đồng bộ trên một queue cụ thể ngay sau khi preload của chúng ta xảy ra, điều này sẽ cho phép chúng ta đợi hoạt động để kết thúc, như thế này:

    ```Swift
    class FileLoaderTests: XCTestCase {
        func testPreloadingFiles() {
            let loader = FileLoader()
            let queue = DispatchQueue(label: "FileLoaderTests")

            loader.preloadFiles(named: ["One", "Two", "Three"], on: queue)

            // Issue an empty closure on the queue and wait for it to be executed
            queue.sync {}

            let preloadedFileNames = loader.preloadedFiles.map { $0.name }
            XCTAssertEqual(preloadedFileNames, ["One", "Two", "Three"])
        }
    }
    ```

    Cũng giống như khi sử dụng expectation, cho phép test của mình tiến hành ngay sau khi tất cả các hoạt động preload kết thúc - không lãng phí thời gian.

# Load View Controllers

## Load View Controller trên Storyboard

- Thêm khai báo import này ở đầu file

    ```@testable import MyViewController```

- Sau đó thêm test case

    ```Swift
    func testLoading() {
        let sb = UIStoryboard(name: "Main", bundle: nil)
        let sut: StoryboardBasedViewController = sb.instantiateViewController(
        identifier: String(describing: StoryboardBasedViewController.self))
    }
    ```

    ## Load View Controller trên XIB

    ```Swift
    func testLoading() {
        let sut = XIBBasedViewController()
    }
    ```

    Một số ví dụ: 

    - Test title:

    ```Swift
    func testTitleIsSettings() {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let vc = storyboard.instantiateViewController(
        withIdentifier: "Settings")
        let _ = vc.view
        XCTAssertEqual(vc.navigationItem.title, "Settings")
    }
    ```

    - Test text UILabel
    ```Swift
    func testLabelTextIsNumberOfResultsToDisplay() {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let vc = storyboard.instantiateViewController(
        withIdentifier: "Settings")
        let _ = vc.view
        XCTAssertEqual(vc.label.text, "Number of results to display")
    }
    ```

    - Test placehodler UITextField
    ```Swift
    XCTAssertEqual(vc.number.placeholder!, "100")
    ```
    - Với file XIB thêm loadViewIfNeeded(). Thao tác này sẽ tải chế độ xem của bộ điều khiển chế độ xem từ XIB, bao gồm cả các ánh xạ.

    ```swift
    func test_loading() {
        let sut = XIBBasedViewController()
        sut.loadViewIfNeeded()
        XCTAssertNotNil(sut.label)
    }
    ```

# UITest trong Xcode

__UITest__ cho phép bạn kiểm tra các tương tác với giao diện người dùng. Kiểm tra giao diện người dùng hoạt động bằng cách tìm các đối tượng giao diện người dùng của ứng dụng bằng các truy vấn, tổng hợp các sự kiện và sau đó gửi các sự kiện đến các đối tượng đó. API cho phép bạn kiểm tra các thuộc tính và trạng thái của đối tượng giao diện người dùng để so sánh chúng với trạng thái mong đợi.

Cách sử dụng:

<img src="https://koenig-media.raywenderlich.com/uploads/2021/03/tests_11_UITests_NewUITestTarget_annotated.png" height="400">

XCUIApplication, XCUIElementQuery, and XCUIElement là ba đối tượng không thể thiếu của UI Testing.

XCUIApplication là đối tượng đại diện cho việc app đang được chạy, và từ nó chúng ta có thể có các truy vấn (query) đê tương tác đến giao diện của app, như sau:

```Swift
let app = XCUIApplication()
app.buttons["show elements"].tap()
```
Phương thức .buttons["show elements"] được gọi là một XCUIElementQuery được cung cấp bởi một đối tượng có kiểu XCUIApplication. Phương thức này sẽ thất bại nếu trong app không có bất kì button nào có tên "show elements". Ngược lại nếu có một button như vậy, phương thức trên sẽ trả về một đối tượng XCUIElement đại diện cho một button. 

1. Tìm element

    ```
    app.alerts.element
    app.buttons.element
    app.collectionViews.element
    app.images.element
    app.maps.element
    app.navigationBars.element
    app.pickers.element
    app.progressIndicators.element
    app.scrollViews.element
    app.segmentedControls.element
    app.staticTexts.element
    app.switches.element
    app.tabBars.element
    app.tables.element
    app.textFields.element
    app.textViews.element
    app.webViews.element
    ```
    Đối với các phần tử cụ thể hơn, hãy đặt identifier truy cập như sau:
    
    ```helpLabel.accessibilityIdentifier = "Help"```

    Tìm kiếm chúng bằng cách

    ```app.staticTexts["Help"]```

2. Truy vấm nâng cao

    XCTest chạy các truy vấn trên giao diện người dùng của ứng dụng, cố gắng tìm một số phần của giao diện người dùng. Mặc dù các truy vấn ở trên là ổn khi bạn mới bắt đầu, nhưng có rất nhiều truy vấn nâng cao mà chúng tôi có thể chạy để tìm các yếu tố cụ thể:

    ```Swift
    // the only button
    app.buttons.element

    // the button titled "Help"
    app.buttons["Help"]

    // all buttons inside a specific scroll view (direct subviews only)
    app.scrollViews["Main"].children(matching: .button)

    // all buttons anywhere inside a specific scroll view (subviews, sub-subviews, sub-sub-subviews, etc)
    app.scrollViews["Main"].descendants(matching: .button)    

    // find the first and fourth buttons
    app.buttons.element(boundBy: 0)
    app.buttons.element(boundBy: 3)

    // find the first button
    app.buttons.firstMatch
    ```

3. Tương tác vói các element

    XCTest cung cấp cho chúng tôi năm cách khác nhau:

    - tap() kích hoạt một lần nhấn, thao tác này sẽ kích hoạt các nút hoặc trường văn bản hiện hoạt để chỉnh sửa. 
    - doubleTap() chạm nhanh hai lần liên tiếp. - twoFingerTap() sử dụng hai ngón tay để chạm một lần vào một phần tử. 
    - tap (withNumberOfTaps: numberOfTouches :) cho phép bạn điều khiển số lần chạm và chạm cùng một lúc. 
    - nhấn (forDuration :) kích hoạt các lần nhấn lâu trên một số giây đã đặt.

    Ngoài ra còn có các phương pháp cụ thể để gesture control.

4. Nhập văn bản

    Có thể kích hoạt một trường văn bản và nhập các chữ cái riêng lẻ vào bàn phím:

    ```Swift
    app.textFields.element.tap()
    app.keys["t"].tap()
    app.keys["e"].tap()
    app.keys["s"].tap()
    app.keys["t"].tap()
    ```

    Ngoài ra, bạn có thể chọn và nhập toàn bộ chuỗi như thế này:

    ```Swift
    app.textFields.element.typeText("test") 
    ```
5. Assertion

    Khi tìm ra element ta có thể sử dụng các assertion thông thường

    ```Swift
    XCTAssertEqual(app.buttons.element.title, "Buy")
    XCTAssertEqual(app.staticTexts.element.label, "test")
    ```

    Kiểm tra xem một element có tồn tại hay không có thể được thực hiện bằng cách sử dụng kiểm tra tồn tại, như sau:

    ```Swift
    XCTAssertTrue(app.alerts["Warning"].exists)
    ```

    Tuy nhiên, thông thường bạn nên chờ một chút thời gian vì bạn đang làm việc với một thiết bị thực - chẳng hạn như hoạt ảnh có thể đang diễn ra. Vì vậy, thay vì sử dụng tồn tại, thay vào đó, chúng ta thường sử dụng waitForExistence (hết thời gian chờ :), như thế này:

    ```Swift
    XCTAssertTrue(app.alerts["Warning"].waitForExistence(timeout: 1))```

    Nếu element đó tồn tại ngay lập tức thì phương thức sẽ trả về ngay lập tức và test success, nhưng nếu không tồn tại thì phương thức sẽ đợi tối đa một giây - nó vẫn thực sự nhanh.

6. Kiểm soát các tests

    Tạo một phiên bản của XCUIApplication không có tham số cho phép bạn kiểm soát bất kỳ ứng dụng nào được chỉ định là “Ứng dụng mục tiêu” trong cài đặt mục tiêu của Xcode. Vì vậy, bạn có thể tạo và khởi chạy ứng dụng mục tiêu của mình như sau:

    ```Swift
    XCUIApplication().launch()
    ```

    Nếu bạn muốn chuyển các đối số cụ thể cho ứng dụng của mình, có lẽ để làm cho ứng dụng thực hiện một số thiết lập dành riêng cho test, bạn có thể thực hiện việc đó bằng cách sử dụng mảng khởi chạy Arguments:

    ``` Swift
    let app = XCUIApplication()
    app.launchArguments = ["enable-testing"]
    app.launch()
    ```

    # Kích hoạt code coverage

    - Để bật mức độ phù hợp của mã, Test action > Gather coverage và tích dưới Options tab:

        <img src="https://koenig-media.raywenderlich.com/uploads/2021/03/tests_15_Coverage_GatherCoverage_annotated-650x351.png" height="300">

    - Chạy tất cả các test với Command-U, sau đó mở report Navigator bằng Command-9. Chọn Coverage trên cùng trong danh sách đó.

        <img src="https://koenig-media.raywenderlich.com/uploads/2021/03/tests_16_Coverage_Log_annotated-650x159.png" height="300">

    - Kết quả

        <img src="https://koenig-media.raywenderlich.com/uploads/2021/02/tests_17_Coverage_Details.png" height="300">





